<?php
/**
 * User: Rashit Hamidullin Rashit.Hamidullin@gmail.com
 * Date: 25.05.14
 * Time: 16:26
 */
include_once ('Reference.php');

class InStock extends Reference
{
    const OUTOFSTOCK = 0;
    const INSTOCK = 1;

    public function __construct()
    {
        $this->list = array(
            self::OUTOFSTOCK => 'отсутствует',
            self::INSTOCK => 'в наличии'
        );
    }
}