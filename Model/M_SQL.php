<?php
//
// Драйвер БД.
//
class MSQL
{
    private $config = array(
        'hostname' => 'localhost',
        'username' => 'root',
        'password' => '',
        'dbName'   => 'onlc'
    );

    public  $mysqli;

    private static $instance;

    public static function Instance()
    {
        if(self::$instance == null)
        {
            self::$instance = new MSQL();
        }
        return self::$instance;
    }

    private function __construct()
    {
        $this->mysqli = new mysqli($this->config['hostname'], $this->config['username'], $this->config['password'], $this->config['dbName']);
        if (mysqli_connect_errno()) {
            printf("Ошибка подключения к базе данных: %s\n", mysqli_connect_error());
            exit();
        }

        if (!$this->mysqli->set_charset("utf8")) {
            printf("Ошибка при загрузке набора символов utf8: %s\n", $this->mysqli->error);
        }
    }

    public function __destruct()
    {
        $this->mysqli->close();
    }

    public function Select($query, $string)
    {
        $stmt = $this->mysqli->prepare($query);

        $f = "%$string%";

        $stmt->bind_param('sss', $f, $f, $f);

        if ($stmt->execute())
        {
            $result = $stmt->get_result();

            while ($row = $result->fetch_assoc()) {
                $arr[] = $row;
            }
        }
        else
        {
            return null;
        }

        return $arr;
    }
}