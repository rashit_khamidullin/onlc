<?php
/**
 * User: Rashit Hamidullin Rashit.Hamidullin@gmail.com
 * Date: 25.05.14
 * Time: 12:45
 * 
 * 
 */
include_once('M_SQL.php');

class M_Goods
{
    private $msql;

    public function __construct()
    {
        $this->msql = MSQL::Instance();
    }

    public function findGoods($string)
    {
        $query = 'SELECT g.* FROM goods g
                    WHERE g.id LIKE ? OR g.name LIKE ? OR g.company_name LIKE ?
                    ORDER BY g.source DESC';

        $goods = $this->msql->Select($query, $string);

        return $goods;
    }
}